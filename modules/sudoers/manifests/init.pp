class sudoers {
    package { 'sudo':
    ensure  => present,
    }

    file { '/etc/sudoers':
        ensure  => file,
        owner   => root,
        group   => root,
        mode    => 0440,
        source  => 'puppet:///modules/sudoers/sudoers',
        require => Package['sudo'],
    }

    file { '/etc/sudoers.d':
        ensure  => directory,
        owner   => root,
        group   => root,
        mode    => 0440,
    }

}
