define sudoers::directive (
  $ensure=present,
  $content="",
  $source=""
) {

  require sudoers
  $dname = regsubst($name, '\.', '-', 'G')

  file {"/etc/sudoers.d/sudo_${dname}":
      ensure  => $ensure ? {
        absent  => absent,
        default => notify  => Exec["sudo-syntax-check for file sudo_$dname"],
      }
      owner   => root,
      group   => root,
      mode    => 0440,
      content => $content ? {
        ""      => undef,
        default => "## NOTICE. This file is managed by Puppet!\n${content}\n",
      },
      source  => $source ? {
        ""      => undef,
        default => $source,
      },
      require => Package["sudo"],
   }

  exec {"sudo-syntax-check for file sudo_$dname":
    command     => "/usr/sbin/visudo -c -f /etc/sudoers.d/sudo_${dname} || ( rm -f /etc/sudoers.d/sudo_${dname} && exit 1)",
    refreshonly => true,
    logoutput => on_failure,
  }

}
