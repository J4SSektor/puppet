<VirtualHost 127.0.0.1:80>
        ServerName test.ru

        DocumentRoot /var/www/test.ru/

        LogLevel debug
        ErrorLog /var/log/apache2/test.ru_error.log
        CustomLog /var/log/apache2/test.ru_access.log combined
        CustomLog "| /usr/sbin/rotatelogs /var/log/apache2-day/test.ru_access.log-%Y-%m-%d 3600 +180" combined


</VirtualHost>
