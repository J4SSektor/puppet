define apache::vhost ($from_template = false,
                      $server_name = $name,
                      $serveraliases = [],
                      $mod_security = "NONE",
                      $document_root = "/var/www/${name}",) {
    include apache    
    
    if ($from_template == true) {
        file { "/etc/apache2/sites-available/${name}":
        content => template("apache/backend.erb"),
        notify  => Exec['apache2_reload'],
        }
    } else {
        file { "/etc/apache2/sites-available/${name}":
        source  => "puppet://${server}/modules/apache/${name}",
        notify  => Exec['apache2_reload'],
        }
    }

    file { "/etc/apache2/sites-enabled/${name}":
        require => File["/etc/apache2/sites-available/${name}"],
        ensure  => link,
        target  => "/etc/apache2/sites-available/${name}",
        notify  => Exec['apache2_reload'],
    }
}
