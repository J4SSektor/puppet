class apache {
    package { "apache2":
        ensure => installed,
        provider => 'aptitude',
    }
    
    file { "/etc/apache2/ports.conf":
        require => Package["apache2"],
        source  => "puppet://${server}/modules/apache/bknd_ports.conf",
    }
    
    file { "/var/www/default":
        require => Package["apache2"],
        ensure  => directory,
        owner   => 'www-data',
    }

    file {"/etc/apache2/sites-available/default":
        require => Package["apache2"],
        content => template("apache/000-default.erb"),
        notify  => Exec['apache2_reload'],
    }

    file {"/etc/apache2/sites-enabled/000-default":
        require => [ Package["apache2"], File["/etc/apache2/sites-available/default"] ],
        ensure => link,
        target => "/etc/apache2/sites-available/default",
        notify  => Exec['apache2_reload'],
    }

    exec { 'apache2_reload':
        command     => '/etc/init.d/apache2 reload',
        refreshonly => true,
    }
}
