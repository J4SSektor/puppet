node "puppet.home.local" {
    notify {$ipaddress:}
    
    apache::vhost { "test.ru": }
    
    apache::vhost { "test4.ru":
        from_template => true,
        server_name => "test4.ru",
        mod_security => "On",
    }
    apache::vhost { "test8.ru":
        from_template => true,
        server_name => "test8.ru",
        serveraliases => ["ololo","trololo"],
        mod_security => "On",
    }
}

